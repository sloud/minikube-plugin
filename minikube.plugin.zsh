install_minikube() {
    local bin="$HOME/.local/bin"
    local completions_dir="$1/src"

    if ! type minikube 2>&1 >/dev/null; then
        mkdir -p "$bin"
        curl -L "https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64" >"$bin/minikube"
        chmod +x "$bin/minikube"

        mkdir -p "$completions_dir"
        minikube completion zsh >"$completions_dir/_minikube"
    fi

    fpath+="$completions_dir"
}

install_minikube "${0:A:h}"
